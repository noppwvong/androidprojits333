package com.example.librarysystem;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

public class Main extends Activity implements OnClickListener {
	boolean customTitleSupported;
	ImageButton btnSearch;
	ImageButton btnDirection;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		customTitleSupported = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.activity_sqlite_data_base);
		Button b1 = (Button) findViewById(R.id.ManageBooks);
		Button b2 = (Button) findViewById(R.id.ManageStaff);

		b1.setOnClickListener(this);
		b2.setOnClickListener(this);

		if (customTitleSupported) {
			getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,
					R.layout.titlebar);
			btnSearch = (ImageButton) findViewById(R.id.btnSearch);
			btnDirection = (ImageButton) findViewById(R.id.btnDirection);

			btnSearch.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Toast.makeText(getBaseContext(), "Go to borrow book.",
							Toast.LENGTH_SHORT).show();
					Intent i = new Intent(getApplicationContext(),
							BorrowManage.class);
					startActivity(i);
				}
			});

			btnDirection.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Toast.makeText(getBaseContext(), "You are at the HomePage",
							Toast.LENGTH_SHORT).show();
				}
			});
		}

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.sqlite_data_base, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int id = v.getId();
		if (id == R.id.ManageBooks) {
			Intent i = new Intent(this, BooksManage.class);
			startActivity(i);
		} else if (id == R.id.ManageStaff) {
			Intent i = new Intent(this, StaffManage.class);
			startActivity(i);
		}
	}
}
