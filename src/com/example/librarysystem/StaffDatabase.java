package com.example.librarysystem;

import android.content.*;
import android.database.sqlite.*;
import android.util.*;

public class StaffDatabase extends SQLiteOpenHelper {
	public static final String TABLE_STAFF = "user";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_Title = "title";
	public static final String COLUMN_NameS = "name";
	public static final String COLUMN_Identification = "IDcard";
	public static final String COLUMN_phone = "phoneNo";
	private static final String DATABASE_NAME = "Staff.db";
	private static final int DATABASE_VERSION = 1;

	private static final String DATABASE_CREATE = "create table " + TABLE_STAFF
			+ "( " + COLUMN_ID + " integer primary key autoincrement, "
			+ COLUMN_Title + " text," + COLUMN_NameS + " text,"
			+ COLUMN_Identification + " numeric," + COLUMN_phone + " numeric"
			+ ");";

	public StaffDatabase(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		database.execSQL(DATABASE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldV, int newV) {
		Log.w(Database.class.getName(), "Upgrading database from version "
				+ oldV + " to " + newV + ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS" + TABLE_STAFF);
		onCreate(db);
	}

}
