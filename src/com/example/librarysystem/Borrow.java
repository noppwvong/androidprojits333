package com.example.librarysystem;

public class Borrow {

	private long id;
	private String bookid;
	private String staffid;
	private String customer;

	public long getId() {
		// Get id.
		return id;
	}

	public void setId(long id) {
		// Set id.
		this.id = id;
	}

	public String getBookid() {
		// Get book id.
		return bookid;
	}

	public void setBookid(String bookid) {
		// Set book id.
		this.bookid = bookid;
	}

	public String getStaffid() {
		// Get staff id.
		return staffid;
	}

	public void setStaffid(String staffid) {
		// Set staff id.
		this.staffid = staffid;
	}

	public String getCustomer() {
		// Get customer name.
		return customer;
	}

	public void setCustomer(String customer) {
		// Set customer name.
		this.customer = customer;
	}

	@Override
	public String toString() {
		return customer;
	}
}
