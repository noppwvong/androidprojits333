package com.example.librarysystem;

import java.util.List;

import android.app.Dialog;
import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import android.app.Activity;
import android.view.Menu;

public class StaffManage extends ListActivity implements OnClickListener {
	final static String TAG = "SQLite";
	private StaffResources datasource;
	List<Staffs> values;

	boolean customTitleSupported;
	ImageButton btnSearch;
	ImageButton btnDirection;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_staff_manage);
		Button add = (Button) findViewById(R.id.save);
		add.setOnClickListener(this);

		if (customTitleSupported) {
			getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,
					R.layout.titlebar);
			btnSearch = (ImageButton) findViewById(R.id.btnSearch);
			btnDirection = (ImageButton) findViewById(R.id.btnDirection);

			btnSearch.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Toast.makeText(getBaseContext(), "Search Clicked !!",
							Toast.LENGTH_SHORT).show();
				}
			});

			btnDirection.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Toast.makeText(getBaseContext(), "Direction Cicked !!",
							Toast.LENGTH_SHORT).show();
				}
			});
		}

		datasource = new StaffResources(this);
		datasource.open();

		showAllStaff();

	}

	private void showAllStaff() {
		// TODO Auto-generated method stub
		values = datasource.getAllStaff();
		ArrayAdapter<Staffs> adapter = new ArrayAdapter<Staffs>(this,
				android.R.layout.simple_list_item_1, values);
		setListAdapter(adapter);
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
		showDetail(position);
	}

	private void showDetail(final int id) {
		// TODO Auto-generated method stub
		@SuppressWarnings("unchecked")
		final ArrayAdapter<Staffs> adapter = (ArrayAdapter<Staffs>) getListAdapter();
		final Staffs staff = (Staffs) getListAdapter().getItem(id);

		final Dialog dialog = new Dialog(StaffManage.this);
		dialog.setContentView(R.layout.staff_detail);
		dialog.setTitle("Staff Detail");
		dialog.setCancelable(true);

		TextView Title = (TextView) dialog.findViewById(R.id.textTitle);
		TextView Name = (TextView) dialog.findViewById(R.id.textName);
		TextView IDcard = (TextView) dialog.findViewById(R.id.textID);
		TextView phoneNo = (TextView) dialog.findViewById(R.id.textphone);

		Title.setText("Title :\t" + staff.getTitle());
		Name.setText("Name :\t" + staff.getName());
		IDcard.setText("Identification :\t" + staff.getIDcard() + "");
		phoneNo.setText("Phone Number :\t" + staff.getphoneNo());

		Button edit = (Button) dialog.findViewById(R.id.button_Edit);
		edit.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (getListAdapter().getCount() > 0) {
					editStaff(staff);
					dialog.dismiss();
				}
			}
		});

		Button button_delete = (Button) dialog.findViewById(R.id.button_Delete);
		button_delete.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (getListAdapter().getCount() > 0) {
					datasource.deleteStaff(staff);
					adapter.remove(staff);
					dialog.dismiss();
					Toast.makeText(StaffManage.this, "Delete data succeed.",
							Toast.LENGTH_LONG).show();
				}
			}
		});

		Button cancel = (Button) dialog.findViewById(R.id.button_Close);
		cancel.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		dialog.show();
	}

	public void editStaff(final Staffs staff) {
		final Dialog dialog = new Dialog(StaffManage.this);
		dialog.setContentView(R.layout.add_staff);
		dialog.setTitle("Edit Staff");
		dialog.setCancelable(true);

		final EditText title = (EditText) dialog
				.findViewById(R.id.editTextTitle);
		final EditText name = (EditText) dialog.findViewById(R.id.editTextName);
		final EditText IDcard = (EditText) dialog.findViewById(R.id.editTextID);
		final EditText phone = (EditText) dialog
				.findViewById(R.id.editTextphone);

		name.setText(staff.getName());
		title.setText(staff.getTitle());
		IDcard.setText(String.valueOf(staff.getIDcard()));
		phone.setText(staff.getphoneNo());

		Button bsave = (Button) dialog.findViewById(R.id.save);
		bsave.setText("Update");
		bsave.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				String values_name = name.getText().toString();
				String values_title = title.getText().toString();
				String values_IDcard = IDcard.getText().toString();
				String values_phone = phone.getText().toString();
				staff.setName(values_name);
				staff.setTitle(values_title);
				staff.setIDcard(values_IDcard);
				staff.setphoneNo(values_phone);
				datasource.updateStaff(staff);
				showAllStaff();
				dialog.cancel();
			}
		});
		Button bcancel = (Button) dialog.findViewById(R.id.button_Cancel);
		bcancel.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				dialog.cancel();
			}
		});
		dialog.show();
	}

	public void addNewStaff() {
		final Dialog dialog = new Dialog(StaffManage.this);
		dialog.setContentView(R.layout.add_staff);
		dialog.setTitle("Add New Staff");
		dialog.setCancelable(true);

		Button button_save = (Button) dialog.findViewById(R.id.save);
		button_save.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				EditText name = (EditText) dialog
						.findViewById(R.id.editTextName);
				EditText title = (EditText) dialog
						.findViewById(R.id.editTextTitle);
				EditText IDcard = (EditText) dialog
						.findViewById(R.id.editTextID);
				EditText phoneNo = (EditText) dialog
						.findViewById(R.id.editTextphone);

				String values_name = name.getText().toString();
				String values_title = title.getText().toString();
				String values_IDCard = IDcard.getText().toString();
				String values_phone = phoneNo.getText().toString();
				@SuppressWarnings("unchecked")
				ArrayAdapter<Staffs> adapter = (ArrayAdapter<Staffs>) getListAdapter();
				Staffs staff = new Staffs();
				staff.setName(values_name);
				staff.setTitle(values_title);
				staff.setIDcard(values_IDCard);
				staff.setphoneNo(values_phone);
				staff = datasource.insertStaff(staff);
				adapter.add(staff);
				dialog.cancel();
			}
		});
		Button bcancel = (Button) dialog.findViewById(R.id.button_Cancel);
		bcancel.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				dialog.cancel();
			}
		});
		dialog.show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		getMenuInflater().inflate(R.menu.staff_manage, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		addNewStaff();

	}

	@Override
	protected void onResume() {
		datasource.open();
		super.onResume();
	}

	@Override
	protected void onPause() {
		datasource.close();
		super.onPause();
	}

}
