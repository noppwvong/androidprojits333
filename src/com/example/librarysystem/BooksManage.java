package com.example.librarysystem;

import java.util.List;

import android.app.Dialog;
import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import android.app.Activity;
import android.view.Menu;

public class BooksManage extends ListActivity implements OnClickListener {
	final static String TAG = "SQLite";
	private BookResources datasource;
	List<Books> values;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_books_manage);
		Button add = (Button) findViewById(R.id.save);
		add.setOnClickListener(this);

		datasource = new BookResources(this);
		datasource.open();

		showAllBook();
	}

	private void showAllBook() {
		// TODO Auto-generated method stub
		values = datasource.getAllBook();
		ArrayAdapter<Books> adapter = new ArrayAdapter<Books>(this,
				android.R.layout.simple_list_item_1, values);
		setListAdapter(adapter);
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
		showDetail(position); // View book by id
	}

	private void showDetail(final int id) {
		// TODO Auto-generated method stub
		@SuppressWarnings("unchecked")
		final ArrayAdapter<Books> adapter = (ArrayAdapter<Books>) getListAdapter();
		final Books book = (Books) getListAdapter().getItem(id);

		final Dialog dialog = new Dialog(BooksManage.this);
		dialog.setContentView(R.layout.book_detail);
		dialog.setTitle("Book Detail");
		dialog.setCancelable(true);

		TextView isbn = (TextView) dialog.findViewById(R.id.textISBN);
		TextView title = (TextView) dialog.findViewById(R.id.textTitle);
		TextView barcode = (TextView) dialog.findViewById(R.id.textPrice);
		TextView publisher = (TextView) dialog.findViewById(R.id.textpublisher);

		isbn.setText("ISBN :\t" + book.getIsbn());
		title.setText("Title :\t" + book.getTitle());
		barcode.setText("Barcode :\t" + book.getPrice() + "");
		publisher.setText("Publisher :\t" + book.getPublisher());

		// edit book
		Button edit = (Button) dialog.findViewById(R.id.button_Edit);
		edit.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (getListAdapter().getCount() > 0) {
					editBook(book);
					dialog.dismiss();
				}
			}
		});
		// delete book
		Button button_delete = (Button) dialog.findViewById(R.id.button_Delete);
		button_delete.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (getListAdapter().getCount() > 0) {
					datasource.deleteBook(book); // delete book
					adapter.remove(book);
					dialog.dismiss();
					Toast.makeText(BooksManage.this, "Delete data succeed.",
							Toast.LENGTH_LONG).show();
				}
			}
		});
		// close dialog
		Button cancel = (Button) dialog.findViewById(R.id.button_Close);
		cancel.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		dialog.show();
	}

	public void editBook(final Books book) {
		final Dialog dialog = new Dialog(BooksManage.this);
		dialog.setContentView(R.layout.add_book);
		dialog.setTitle("Edit Book");
		dialog.setCancelable(true);

		final EditText isbn = (EditText) dialog.findViewById(R.id.editText);
		final EditText title = (EditText) dialog.findViewById(R.id.password);
		final EditText price = (EditText) dialog.findViewById(R.id.editText3);
		final EditText publisher = (EditText) dialog
				.findViewById(R.id.editText4);

		isbn.setText(book.getIsbn());
		title.setText(book.getTitle());
		price.setText(String.valueOf(book.getPrice()));
		publisher.setText(book.getPublisher());

		Button bsave = (Button) dialog.findViewById(R.id.save);
		bsave.setText("Update");
		bsave.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				String values_isbn = isbn.getText().toString();
				String values_title = title.getText().toString();
				double values_price = Double.parseDouble(price.getText()
						.toString());
				String values_publisher = publisher.getText().toString();
				book.setIsbn(values_isbn);
				book.setTitle(values_title);
				book.setPrice(values_price);
				book.setPublisher(values_publisher);
				datasource.updateBook(book);
				showAllBook();
				dialog.cancel();
			}
		});
		Button bcancel = (Button) dialog.findViewById(R.id.button_Cancel);
		bcancel.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				dialog.cancel();
			}
		});
		dialog.show();
	}

	public void addNewBook() {
		final Dialog dialog = new Dialog(BooksManage.this);
		dialog.setContentView(R.layout.add_book);
		dialog.setTitle("Add New Book");
		dialog.setCancelable(true);
		// setup button
		Button button_save = (Button) dialog.findViewById(R.id.save);
		button_save.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// get string
				EditText isbn = (EditText) dialog.findViewById(R.id.editText);
				EditText title = (EditText) dialog.findViewById(R.id.password);
				EditText price = (EditText) dialog.findViewById(R.id.editText3);
				EditText publisher = (EditText) dialog
						.findViewById(R.id.editText4);

				String values_isbn = isbn.getText().toString();
				String values_title = title.getText().toString();
				double values_price = Double.parseDouble(price.getText()
						.toString());
				String values_publisher = publisher.getText().toString();
				@SuppressWarnings("unchecked")
				ArrayAdapter<Books> adapter = (ArrayAdapter<Books>) getListAdapter();
				Books book = new Books();
				book.setIsbn(values_isbn);
				book.setTitle(values_title);
				book.setPrice(values_price);
				book.setPublisher(values_publisher);
				book = datasource.insertBook(book);
				adapter.add(book);
				dialog.cancel();
			}
		});
		Button bcancel = (Button) dialog.findViewById(R.id.button_Cancel);
		bcancel.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				dialog.cancel();
			}
		});
		dialog.show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.books_manage, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		addNewBook();

	}

	@Override
	protected void onResume() {
		datasource.open();
		super.onResume();
	}

	@Override
	protected void onPause() {
		datasource.close();
		super.onPause();
	}

}
