package com.example.librarysystem;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class BookResources {
	public static final String TAG = "SQLite Database";
	private SQLiteDatabase database;
	private Database dbHelper;
	private String[] allColumns = { Database.COLUMN_ID, Database.COLUMN_ISBN,
			Database.COLUMN_TITLE, Database.COLUMN_PRICE,
			Database.COLUMN_PUBISHER };

	public BookResources(Context context) {
		dbHelper = new Database(context);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}

	public Books insertBook(Books book) {
		ContentValues values = new ContentValues();
		values.put(Database.COLUMN_ISBN, book.getIsbn());
		values.put(Database.COLUMN_TITLE, book.getTitle());
		values.put(Database.COLUMN_PRICE, book.getPrice());
		values.put(Database.COLUMN_PUBISHER, book.getPublisher());
		long insertId = database.insert(Database.TABLE_BOOKS, null, values);

		Cursor cursor = database.query(Database.TABLE_BOOKS, allColumns,
				Database.COLUMN_ID + " = " + insertId, null, null, null, null);
		cursor.moveToFirst();
		return cursorToBook(cursor);
	}

	public void deleteBook(Books book) {
		long id = book.getId();
		Log.d(TAG, "Book deleted with id: " + id);
		database.delete(Database.TABLE_BOOKS, Database.COLUMN_ID + " = " + id,
				null);
	}

	public void updateBook(Books book) {
		ContentValues v = new ContentValues();
		v.put(Database.COLUMN_ISBN, book.getIsbn());
		v.put(Database.COLUMN_TITLE, book.getTitle());
		v.put(Database.COLUMN_PRICE, book.getPrice());
		v.put(Database.COLUMN_PUBISHER, book.getPublisher());
		database.update(Database.TABLE_BOOKS, v, Database.COLUMN_ID + "="
				+ book.getId(), null);
	}

	public List<Books> getAllBook() {
		List<Books> comments = new ArrayList<Books>();
		Cursor cursor = database.query(Database.TABLE_BOOKS, allColumns, null,
				null, null, null, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Books comment = cursorToBook(cursor);
			comments.add(comment);
			cursor.moveToNext();
		}
		cursor.close();
		return comments;
	}

	private Books cursorToBook(Cursor cursor) {
		Books book = new Books();
		book.setId(cursor.getLong(0));
		book.setIsbn(cursor.getString(1));
		book.setTitle(cursor.getString(2));
		book.setPrice(cursor.getDouble(3));
		book.setPublisher(cursor.getString(4));
		return book;
	}
}
