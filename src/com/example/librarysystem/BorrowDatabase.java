package com.example.librarysystem;

import android.content.*;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class BorrowDatabase extends SQLiteOpenHelper {

	public static final String TABLE_BORROW = "borrow";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_BOOKID = "bookid";
	public static final String COLUMN_STAFFID = "staffid";
	public static final String COLUMN_CUSTOMER = "customer";
	private static final String DATABASE_NAME = "Borrow.db";
	private static final int DATABASE_VERSION = 1;

	private static final String DATABASE_CREATE = "create table "
			+ TABLE_BORROW + "( " + COLUMN_ID
			+ " integer primary key autoincrement, " + COLUMN_BOOKID + " text,"
			+ COLUMN_STAFFID + " integer," + COLUMN_CUSTOMER + " text" + ");";

	public BorrowDatabase(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		// TODO Auto-generated method stub
		database.execSQL(DATABASE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldV, int newV) {
		// TODO Auto-generated method stub
		Log.w(Database.class.getName(), "Upgrading database from version "
				+ oldV + " to " + newV + ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS" + TABLE_BORROW);
		onCreate(db);
	}
}
