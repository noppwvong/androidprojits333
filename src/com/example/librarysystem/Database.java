package com.example.librarysystem;

import android.content.*;
import android.database.sqlite.*;
import android.util.*;

public class Database extends SQLiteOpenHelper {
	public static final String TABLE_BOOKS = "book";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_ISBN = "isbn";
	public static final String COLUMN_TITLE = "title";
	public static final String COLUMN_PRICE = "price";
	public static final String COLUMN_PUBISHER = "publisher";
	private static final String DATABASE_NAME = "Allbooks.db";
	private static final int DATABASE_VERSION = 1;

	private static final String DATABASE_CREATE = "create table " + TABLE_BOOKS
			+ "( " + COLUMN_ID + " integer primary key autoincrement, "
			+ COLUMN_ISBN + " text," + COLUMN_TITLE + " text," + COLUMN_PRICE
			+ " numeric," + COLUMN_PUBISHER + " text" + ");";

	public Database(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		database.execSQL(DATABASE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldV, int newV) {
		Log.w(Database.class.getName(), "Upgrading database from version "
				+ oldV + " to " + newV + ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS" + TABLE_BOOKS);
		onCreate(db);
	}

}
