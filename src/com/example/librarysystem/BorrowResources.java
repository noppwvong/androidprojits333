package com.example.librarysystem;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class BorrowResources {

	public static final String TAG = "SQLite Database";
	private SQLiteDatabase database;
	private BorrowDatabase dbHelper;
	private String[] allColumns = { BorrowDatabase.COLUMN_ID,
			BorrowDatabase.COLUMN_BOOKID, BorrowDatabase.COLUMN_STAFFID,
			BorrowDatabase.COLUMN_CUSTOMER };

	public BorrowResources(Context context) {
		dbHelper = new BorrowDatabase(context);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}

	public Borrow insertBorrow(Borrow borrow) {
		ContentValues values = new ContentValues();
		values.put(BorrowDatabase.COLUMN_BOOKID, borrow.getBookid());
		values.put(BorrowDatabase.COLUMN_STAFFID, borrow.getStaffid());
		values.put(BorrowDatabase.COLUMN_CUSTOMER, borrow.getCustomer());
		long insertId = database.insert(BorrowDatabase.TABLE_BORROW, null,
				values);

		Cursor cursor = database.query(BorrowDatabase.TABLE_BORROW, allColumns,
				BorrowDatabase.COLUMN_ID + " = " + insertId, null, null, null,
				null);
		cursor.moveToFirst();
		return cursorToBorrow(cursor);
	}

	public void deleteBorrow(Borrow borrow) {
		long id = borrow.getId();
		Log.d(TAG, "Borrow list deleted with id: " + id);
		database.delete(BorrowDatabase.TABLE_BORROW, BorrowDatabase.COLUMN_ID
				+ " = " + id, null);
	}

	public void updateBorrow(Borrow borrow) {
		ContentValues v = new ContentValues();

		v.put(BorrowDatabase.COLUMN_BOOKID, borrow.getBookid());
		v.put(BorrowDatabase.COLUMN_STAFFID, borrow.getStaffid());
		v.put(BorrowDatabase.COLUMN_CUSTOMER, borrow.getCustomer());
		database.update(BorrowDatabase.TABLE_BORROW, v, StaffDatabase.COLUMN_ID
				+ "=" + borrow.getId(), null);
	}

	public List<Borrow> getAllBorrow() {
		List<Borrow> comments = new ArrayList<Borrow>();
		Cursor cursor = database.query(BorrowDatabase.TABLE_BORROW, allColumns,
				null, null, null, null, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Borrow comment = cursorToBorrow(cursor);
			comments.add(comment);
			cursor.moveToNext();
		}
		cursor.close();
		return comments;
	}

	private Borrow cursorToBorrow(Cursor cursor) {
		Borrow borrow = new Borrow();
		borrow.setId(cursor.getLong(0));
		borrow.setBookid(cursor.getString(1));
		borrow.setStaffid(cursor.getString(2));
		borrow.setCustomer(cursor.getString(3));
		return borrow;
	}
}
