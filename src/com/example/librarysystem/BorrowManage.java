package com.example.librarysystem;

import java.util.List;

import android.os.Bundle;
import android.app.Activity;
import android.app.Dialog;
import android.app.ListActivity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class BorrowManage extends ListActivity implements OnClickListener {

	final static String TAG = "SQLite";
	private BorrowResources datasource;
	List<Borrow> values;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_borrow_manage);
		Button add = (Button) findViewById(R.id.borrowadd);
		add.setOnClickListener(this);

		datasource = new BorrowResources(this);
		datasource.open(); // Open connection.

		showAllBorrow(); // Call "showAllBorrow()".
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
		showDetail(position); // View borrow data by id when user clicked on
								// each listitem.
	}

	private void showAllBorrow() {
		// TODO Auto-generated method stub
		values = datasource.getAllBorrow();
		ArrayAdapter<Borrow> adapter = new ArrayAdapter<Borrow>(this,
				android.R.layout.simple_list_item_1, values);
		setListAdapter(adapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.borrow_manage, menu);
		return true;
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		addNewBorrow(); // Call "addNewBorrow()" when user clicked on a button
						// labeled "Add".
	}

	private void showDetail(final int id) {
		// Display details of each item.
		// TODO Auto-generated method stub
		@SuppressWarnings("unchecked")
		final ArrayAdapter<Borrow> adapter = (ArrayAdapter<Borrow>) getListAdapter();
		final Borrow borrow = (Borrow) getListAdapter().getItem(id);

		final Dialog dialog = new Dialog(BorrowManage.this); // Create a dialog.
		dialog.setContentView(R.layout.borrow_detail);
		dialog.setTitle("Borrowed Book Details"); // Set name of dialog.
		dialog.setCancelable(true);

		TextView isbn = (TextView) dialog.findViewById(R.id.bbookid);
		TextView staff = (TextView) dialog.findViewById(R.id.bstaffid);
		TextView customer = (TextView) dialog.findViewById(R.id.bcustomername);

		isbn.setText(" Book id :\t" + borrow.getBookid()); // Show book id.
		staff.setText(" Staff id :\t" + borrow.getStaffid()); // Show staff id.
		customer.setText(" Customer name :\t" + borrow.getCustomer() + ""); // Show
																			// customer
																			// name.

		// edit book
		Button edit = (Button) dialog.findViewById(R.id.button_Edit);
		edit.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (getListAdapter().getCount() > 0) {
					editBorrow(borrow);
					dialog.dismiss();
				}
			}
		});
		// delete borrow
		Button button_delete = (Button) dialog.findViewById(R.id.button_Delete);
		button_delete.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (getListAdapter().getCount() > 0) {
					datasource.deleteBorrow(borrow); // delete book
					adapter.remove(borrow);
					dialog.dismiss();
					Toast.makeText(BorrowManage.this, "Delete data succeed.",
							Toast.LENGTH_LONG).show();
				}
			}
		});
		// close dialog
		Button cancel = (Button) dialog.findViewById(R.id.button_Close);
		cancel.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		dialog.show(); // Show dialog.
	}

	public void editBorrow(final Borrow borrow) {
		// Edit data and update database.
		final Dialog dialog = new Dialog(BorrowManage.this);
		dialog.setContentView(R.layout.add_borrow);
		dialog.setTitle("Edit Borrowed Books");
		dialog.setCancelable(true);

		final EditText isbn = (EditText) dialog.findViewById(R.id.bookid);
		final EditText staff = (EditText) dialog.findViewById(R.id.staffID);
		final EditText customer = (EditText) dialog
				.findViewById(R.id.customername);

		isbn.setText(borrow.getBookid());
		staff.setText(borrow.getStaffid());
		customer.setText(borrow.getCustomer());

		Button bsave = (Button) dialog.findViewById(R.id.save);
		bsave.setText("Update"); // Set name of dialog.
		bsave.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				String values_isbn = isbn.getText().toString();
				String values_staff = staff.getText().toString();
				String values_customer = customer.getText().toString();
				borrow.setBookid(values_isbn); // Set book id.
				borrow.setStaffid(values_staff); // set staff id.
				borrow.setCustomer(values_customer); // Set customer name.
				datasource.updateBorrow(borrow);
				showAllBorrow();
				dialog.cancel(); // Close dialog.
			}
		});
		Button bcancel = (Button) dialog.findViewById(R.id.button_Cancel);
		bcancel.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				dialog.cancel(); // Close dialog.
			}
		});
		dialog.show(); // Show dialog.
	}

	public void addNewBorrow() {
		// Insert new data into database.
		final Dialog dialog = new Dialog(BorrowManage.this); // Create new
																// dialog.
		dialog.setContentView(R.layout.add_borrow);
		dialog.setTitle("Add New Borrow"); // Set name of dialog.
		dialog.setCancelable(true);
		// setup button
		Button button_save = (Button) dialog.findViewById(R.id.save);
		button_save.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// get string
				EditText isbn = (EditText) dialog.findViewById(R.id.bookid);
				EditText staff = (EditText) dialog.findViewById(R.id.staffID);
				EditText customer = (EditText) dialog
						.findViewById(R.id.customername);

				String values_isbn = isbn.getText().toString();
				String values_title = staff.getText().toString();
				String values_customer = customer.getText().toString();
				@SuppressWarnings("unchecked")
				ArrayAdapter<Borrow> adapter = (ArrayAdapter<Borrow>) getListAdapter();
				Borrow borrow = new Borrow();
				borrow.setBookid(values_isbn); // Set book id.
				borrow.setStaffid(values_title); // Set staff id.
				borrow.setCustomer(values_customer); // Set name of customer.
				borrow = datasource.insertBorrow(borrow); // Insert these data
															// into table.
				adapter.add(borrow);
				dialog.cancel(); // Close dialog.
			}
		});
		Button bcancel = (Button) dialog.findViewById(R.id.button_Cancel);
		bcancel.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				dialog.cancel();
			}
		});
		dialog.show(); // Show dialog.
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		datasource.close();
		super.onPause();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		datasource.open();
		super.onResume();
	}
}
