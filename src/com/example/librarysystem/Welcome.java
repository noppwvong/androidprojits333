package com.example.librarysystem;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

import android.os.Handler;
import android.view.Window;

public class Welcome extends Activity {
	Handler handler;
	Runnable runnable;
	Long delay_time;
	Long time = 3000L;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_welcome);
		handler = new Handler();

		runnable = new Runnable() {
			public void run() {
				Intent intent = new Intent(Welcome.this, Main.class);
				startActivity(intent);
				finish();
			}
		};
	}

	public void onResume() {
		super.onResume();
		delay_time = time;
		handler.postDelayed(runnable, delay_time);
		time = System.currentTimeMillis();
	}

	public void onPause() {
		super.onPause();
		handler.removeCallbacks(runnable);
		time = delay_time - (System.currentTimeMillis() - time);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.welcome, menu);
		return true;
	}

}
