package com.example.librarysystem;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class StaffResources {
	public static final String TAG = "SQLite Database";
	private SQLiteDatabase database;
	private StaffDatabase dbHelper;
	private String[] allColumns = { StaffDatabase.COLUMN_ID,
			StaffDatabase.COLUMN_Title, StaffDatabase.COLUMN_NameS,
			StaffDatabase.COLUMN_Identification, StaffDatabase.COLUMN_phone };

	public StaffResources(Context context) {
		dbHelper = new StaffDatabase(context);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}

	public Staffs insertStaff(Staffs staff) {
		ContentValues values = new ContentValues();
		values.put(StaffDatabase.COLUMN_Title, staff.getTitle());
		values.put(StaffDatabase.COLUMN_NameS, staff.getName());
		values.put(StaffDatabase.COLUMN_Identification, staff.getIDcard());
		values.put(StaffDatabase.COLUMN_phone, staff.getphoneNo());
		long insertId = database
				.insert(StaffDatabase.TABLE_STAFF, null, values);

		Cursor cursor = database.query(StaffDatabase.TABLE_STAFF, allColumns,
				StaffDatabase.COLUMN_ID + " = " + insertId, null, null, null,
				null);
		cursor.moveToFirst();
		return cursorToStaff(cursor);
	}

	public void deleteStaff(Staffs staff) {
		long id = staff.getId();
		Log.d(TAG, "Staff deleted with id: " + id);
		database.delete(StaffDatabase.TABLE_STAFF, StaffDatabase.COLUMN_ID
				+ " = " + id, null);
	}

	public void updateStaff(Staffs staff) {
		ContentValues v = new ContentValues();

		v.put(StaffDatabase.COLUMN_Title, staff.getTitle());
		v.put(StaffDatabase.COLUMN_NameS, staff.getName());
		v.put(StaffDatabase.COLUMN_Identification, staff.getIDcard());
		v.put(StaffDatabase.COLUMN_phone, staff.getphoneNo());
		database.update(StaffDatabase.TABLE_STAFF, v, StaffDatabase.COLUMN_ID
				+ "=" + staff.getId(), null);
	}

	public List<Staffs> getAllStaff() {
		List<Staffs> comments = new ArrayList<Staffs>();
		Cursor cursor = database.query(StaffDatabase.TABLE_STAFF, allColumns,
				null, null, null, null, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Staffs comment = cursorToStaff(cursor);
			comments.add(comment);
			cursor.moveToNext();
		}
		cursor.close();
		return comments;
	}

	private Staffs cursorToStaff(Cursor cursor) {
		Staffs staff = new Staffs();
		staff.setId(cursor.getLong(0));
		staff.setTitle(cursor.getString(1));
		staff.setName(cursor.getString(2));
		staff.setIDcard(cursor.getString(3));
		staff.setphoneNo(cursor.getString(4));
		return staff;
	}
}
