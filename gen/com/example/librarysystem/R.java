/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.example.librarysystem;

public final class R {
    public static final class attr {
        /** <p>Must be a reference to another resource, in the form "<code>@[+][<i>package</i>:]<i>type</i>:<i>name</i></code>"
or to a theme attribute in the form "<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>".
         */
        public static final int buttonBarButtonStyle=0x7f010001;
        /** <p>Must be a reference to another resource, in the form "<code>@[+][<i>package</i>:]<i>type</i>:<i>name</i></code>"
or to a theme attribute in the form "<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>".
         */
        public static final int buttonBarStyle=0x7f010000;
    }
    public static final class color {
        public static final int black_overlay=0x7f040000;
    }
    public static final class dimen {
        /**  Default screen margins, per the Android Design guidelines. 

         Customize dimensions originally defined in res/values/dimens.xml (such as
         screen margins) for sw720dp devices (e.g. 10" tablets) in landscape here.
    
         */
        public static final int activity_horizontal_margin=0x7f050000;
        public static final int activity_vertical_margin=0x7f050001;
    }
    public static final class drawable {
        public static final int applogo=0x7f020000;
        public static final int bg1=0x7f020001;
        public static final int bg2=0x7f020002;
        public static final int bg_titlebar=0x7f020003;
        public static final int home=0x7f020004;
        public static final int home1=0x7f020005;
        public static final int ic_action_user_add=0x7f020006;
        public static final int ic_launcher=0x7f020007;
        public static final int imagebutton=0x7f020008;
        public static final int imagebuttond=0x7f020009;
        public static final int imagebuttonp=0x7f02000a;
        public static final int red=0x7f02000b;
        public static final int staffbutton=0x7f02000c;
        public static final int welcome=0x7f02000d;
    }
    public static final class id {
        public static final int ManageBooks=0x7f090004;
        public static final int ManageStaff=0x7f090003;
        public static final int action_forgot_password=0x7f09002d;
        public static final int action_settings=0x7f09002c;
        public static final int bbookid=0x7f09001c;
        public static final int bcustomername=0x7f09001e;
        public static final int bookid=0x7f09000c;
        public static final int borrowadd=0x7f090002;
        public static final int borrowcustomerid=0x7f09000f;
        public static final int borrowisbn=0x7f09000b;
        public static final int borrowstaffid=0x7f09000d;
        public static final int bstaffid=0x7f09001d;
        public static final int btnDirection=0x7f09002b;
        public static final int btnSearch=0x7f09002a;
        public static final int button2=0x7f090023;
        public static final int button_Cancel=0x7f09000a;
        public static final int button_Close=0x7f09001b;
        public static final int button_Delete=0x7f09001a;
        public static final int button_Edit=0x7f090019;
        public static final int customername=0x7f090010;
        public static final int editText=0x7f090005;
        public static final int editText3=0x7f090007;
        public static final int editText4=0x7f090008;
        public static final int editTextID=0x7f090013;
        public static final int editTextName=0x7f090012;
        public static final int editTextTitle=0x7f090011;
        public static final int editTextphone=0x7f090014;
        public static final int group=0x7f090000;
        public static final int imageView1=0x7f090028;
        public static final int linearLayout1=0x7f090009;
        public static final int linearLayout3=0x7f090029;
        public static final int password=0x7f090006;
        public static final int save=0x7f090001;
        public static final int staffID=0x7f09000e;
        public static final int start=0x7f090022;
        public static final int tableRow1=0x7f090027;
        public static final int textID=0x7f090025;
        public static final int textISBN=0x7f090015;
        public static final int textName=0x7f090024;
        public static final int textPrice=0x7f090017;
        public static final int textTitle=0x7f090016;
        public static final int textView1=0x7f09001f;
        public static final int textView2=0x7f090021;
        public static final int textphone=0x7f090026;
        public static final int textpublisher=0x7f090018;
        public static final int user=0x7f090020;
    }
    public static final class layout {
        public static final int activity_books_manage=0x7f030000;
        public static final int activity_borrow_manage=0x7f030001;
        public static final int activity_sqlite_data_base=0x7f030002;
        public static final int activity_staff_manage=0x7f030003;
        public static final int activity_welcome=0x7f030004;
        public static final int add_book=0x7f030005;
        public static final int add_borrow=0x7f030006;
        public static final int add_staff=0x7f030007;
        public static final int book_detail=0x7f030008;
        public static final int borrow_detail=0x7f030009;
        public static final int login_activity=0x7f03000a;
        public static final int staff_detail=0x7f03000b;
        public static final int titlebar=0x7f03000c;
    }
    public static final class menu {
        public static final int books_manage=0x7f080000;
        public static final int borrow_manage=0x7f080001;
        public static final int login=0x7f080002;
        public static final int sqlite_data_base=0x7f080003;
        public static final int staff_manage=0x7f080004;
        public static final int welcome=0x7f080005;
    }
    public static final class string {
        public static final int action_forgot_password=0x7f060014;
        public static final int action_settings=0x7f060001;
        public static final int action_sign_in_register=0x7f060012;
        public static final int action_sign_in_short=0x7f060013;
        public static final int add_borrow=0x7f060006;
        public static final int app_name=0x7f060000;
        public static final int borrowbookid=0x7f060007;
        public static final int borrowcustomername=0x7f060009;
        public static final int borrowstaffid=0x7f060008;
        public static final int cancelbutton=0x7f06000b;
        public static final int dummy_button=0x7f06000d;
        public static final int dummy_content=0x7f06000e;
        public static final int error_field_required=0x7f060019;
        public static final int error_incorrect_password=0x7f060018;
        public static final int error_invalid_email=0x7f060016;
        public static final int error_invalid_password=0x7f060017;
        public static final int hello_world=0x7f060002;
        public static final int login_progress_signing_in=0x7f060015;
        /**  Strings related to login 
         */
        public static final int prompt_email=0x7f060010;
        public static final int prompt_password=0x7f060011;
        public static final int savebutton=0x7f06000a;
        public static final int title_activity_books_manage=0x7f060003;
        public static final int title_activity_borrow_manage=0x7f060005;
        public static final int title_activity_login=0x7f06000f;
        public static final int title_activity_staff_manage=0x7f060004;
        public static final int title_activity_welcome=0x7f06000c;
    }
    public static final class style {
        /** 
        Base application theme for API 11+. This theme completely replaces
        AppBaseTheme from res/values/styles.xml on API 11+ devices.
    
 API 11 theme customizations can go here. 

        Base application theme for API 14+. This theme completely replaces
        AppBaseTheme from BOTH res/values/styles.xml and
        res/values-v11/styles.xml on API 14+ devices.
    
 API 14 theme customizations can go here. 
         */
        public static final int AppBaseTheme=0x7f070005;
        public static final int ButtonBar=0x7f070003;
        public static final int ButtonBarButton=0x7f070004;
        public static final int ButtonText=0x7f070001;
        public static final int FullscreenActionBarStyle=0x7f070006;
        public static final int FullscreenTheme=0x7f070002;
        public static final int LoginFormContainer=0x7f070007;
        public static final int bgTitlebar=0x7f070000;
        public static final int customTitlebar=0x7f070008;
    }
    public static final class styleable {
        /** 
         Declare custom theme attributes that allow changing which styles are
         used for button bars depending on the API level.
         ?android:attr/buttonBarStyle is new as of API 11 so this is
         necessary to support previous API levels.
    
           <p>Includes the following attributes:</p>
           <table>
           <colgroup align="left" />
           <colgroup align="left" />
           <tr><th>Attribute</th><th>Description</th></tr>
           <tr><td><code>{@link #ButtonBarContainerTheme_buttonBarButtonStyle com.example.librarysystem:buttonBarButtonStyle}</code></td><td></td></tr>
           <tr><td><code>{@link #ButtonBarContainerTheme_buttonBarStyle com.example.librarysystem:buttonBarStyle}</code></td><td></td></tr>
           </table>
           @see #ButtonBarContainerTheme_buttonBarButtonStyle
           @see #ButtonBarContainerTheme_buttonBarStyle
         */
        public static final int[] ButtonBarContainerTheme = {
            0x7f010000, 0x7f010001
        };
        /**
          <p>This symbol is the offset where the {@link com.example.librarysystem.R.attr#buttonBarButtonStyle}
          attribute's value can be found in the {@link #ButtonBarContainerTheme} array.


          <p>Must be a reference to another resource, in the form "<code>@[+][<i>package</i>:]<i>type</i>:<i>name</i></code>"
or to a theme attribute in the form "<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>".
          @attr name com.example.librarysystem:buttonBarButtonStyle
        */
        public static final int ButtonBarContainerTheme_buttonBarButtonStyle = 1;
        /**
          <p>This symbol is the offset where the {@link com.example.librarysystem.R.attr#buttonBarStyle}
          attribute's value can be found in the {@link #ButtonBarContainerTheme} array.


          <p>Must be a reference to another resource, in the form "<code>@[+][<i>package</i>:]<i>type</i>:<i>name</i></code>"
or to a theme attribute in the form "<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>".
          @attr name com.example.librarysystem:buttonBarStyle
        */
        public static final int ButtonBarContainerTheme_buttonBarStyle = 0;
    };
}
